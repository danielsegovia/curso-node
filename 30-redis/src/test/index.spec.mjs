import app from '../app.mjs'
import request from 'supertest'
import { faker } from '@faker-js/faker';
import 'dotenv/config'

describe("APP", () => {
    test("testo si la app esta viva", async () => {
        const response = await request(app).get('/ping').send()
        expect(response.status).toBe(200)
    })
})

const payload = {
    firstname: faker.internet.userName(),
    lastname: faker.internet.userName(),
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
}

describe("Auth Module", () => {

    test("register test", async () => {
        const response = await request(app)
            .post('/auth/register')
            .set("Content-type", "application/json")
            .send(payload)

        expect(response.status).toBe(201)
        expect(response.body.user._id).toBeDefined()
    })

    test("username duplicated", async () => {
        //envio mismo payload para probar un usuario duplicado
        const response = await request(app)
            .post('/auth/register')
            .set("Content-type", "application/json")
            .send(payload)
        expect(response.status).toBe(409)
    })

    test("login", async () => {
        //envio mismo payload para probar un usuario duplicado
        const response = await request(app)
            .post('/auth/login')
            .set("Content-type", "application/json")
            .send(payload)
        expect(response.status).toBe(200)
    })

    test("register with no data", async () => {
        const response = await request(app)
            .post('/auth/register')

        expect(response.status).toBe(500)
    })
})

describe("Product Module", () => {
    let token
    let product_id

    beforeAll(async () => {

        const response = await request(app).post('/auth/login')
            .set("Content-type", "application/json")
            .send(payload)
        token = response.body.token;
    });

    test("add product", async () => {
        const payload = {
            title: faker.commerce.productName(),
            description: faker.lorem.paragraph()
        }

        const response = await request(app)
            .post('/products')
            .set("Content-type", "application/json")
            .set('Authorization', `Bearer ${token}`)
            .send(payload)

        expect(response.status).toBe(201)
        expect(response.body.product._id).toBeDefined()

        product_id = response.body.product._id
    })

    test("get all products", async () => {

        const response = await request(app)
            .get('/products')
            .set("Content-type", "application/json")
            .set('Authorization', `Bearer ${token}`)
            .send()

        expect(response.status).toBe(200)
        expect(response.body.products).toEqual(expect.any(Array))
    })

    test("get one product", async () => {

        const response = await request(app)
            .get('/products/' + product_id)
            .set("Content-type", "application/json")
            .set('Authorization', `Bearer ${token}`)
            .send()

        expect(response.status).toBe(200)
        expect(response.body.products).toEqual(expect.any(Object))
        expect(response.body.products._id).toBeDefined()
        
    })

    test("get products without token", async () => {

        const response = await request(app)
            .get('/products')
            .set("Content-type", "application/json")
        //    .set('Authorization', `Bearer ${token}`)
            .send()

        expect(response.status).toBe(401)
        
    })

    test("get products with a wrong token", async () => {

        const response = await request(app)
            .get('/products')
            .set("Content-type", "application/json")
            .set('Authorization', `Bearer 123456789`)
            .send()

        expect(response.status).toBe(401)
        
    })
})