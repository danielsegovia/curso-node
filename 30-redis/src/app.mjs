import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";
import 'dotenv/config'
import mongoose from './config/mongo.mjs';
import redis from './config/redis.mjs';
import express from 'express';
const app = express();
app.use(express.json())

Sentry.init({
    dsn: process.env.SENTRY_INIT
  });


app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());



app.get('/ping', (req, res) => {
    res.status(200).json({msj: 'API is running'})
})

import usersRouter from './ruotes/users.mjs';
app.use('/users', usersRouter);
import authRouter from './ruotes/auth.mjs';
app.use('/auth', authRouter);
import productsRouter from './ruotes/products.mjs';
app.use('/products', productsRouter);


// Handling non matching request from the client
app.use((req, res, next) => {
    try {
        throw new Error("Page not found")
      } catch (error) {
        next(error)
      }
    
})

app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
  res.status(500).json({msj: err.message});
});

export default app