import texts from './textGenerator.mjs'
import mailchimp from '../config/mailchimp.mjs'

export default {
    sendWelcomeMessage : async (name) => {
        try {
            const body = await texts.getWelcomeMessage(name)

            const message = {
                from_email: "info@xumby.com",
                subject: "Bienvenido al curso de Node.js",
                text: body,
                to: [
                  {
                    email: "dani@xumby.com",
                    type: "to"
                  }
                ]
              };

              const response = await mailchimp.messages.send({message});

              if(response[0].status !== 'sent'){
                console.log("El email no ha podido ser enviado")
                throw new Error(JSON.stringify(response))
              }

              console.log("se ha enviado el email")

              return true;
            
        
        } catch (error) {
            console.error(error)
            throw new Error(error)
        }
    }
}