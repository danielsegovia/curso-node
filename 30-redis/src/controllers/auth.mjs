import UserModel from '../models/users.mjs';
import 'dotenv/config'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken';
import mailing from '../services/mailing.mjs';
import KafkaConfig from '../config/kafka.mjs'

const register = async (req, res, next) => {
  try {
    
    let {firstname, lastname, email, username, password} = req.body
    const newUser = new UserModel({firstname, lastname, email, username, password})
    const user = await newUser.save()

    /* const response = mailing.sendWelcomeMessage(user.firstname)
    if(!response) throw new Error(`El email de bienvenida a ${user._id} no se ha podido enviar`)
    */
   
    const kafkaConfig = new KafkaConfig();
    const messages = [{ key: email, value: JSON.stringify(user)}];
    kafkaConfig.produce("users", messages);



    return res.status(201).json({ msj: "El usuario ha sido agregado", user});
  } catch (error) {
    next(error)
  }
};

const login = async (req, res) => {
  try {
    let {username, password} = req.body
    
    const user = await UserModel.findOne({username: username}).select('_id username password firstname lastname email')
    if(!user) return res.status(401).json({ msj: "Credenciales inválidas"});

    let logged = await user.comparePassword(password)
    if(!logged) return res.status(401).json({ msj: "Credenciales inválidas"});

    const token = jwt.sign(
      { username: user.username, id: user._id },
      process.env.TOKEN_SECRET)

    return res.status(200).json({ msj: "login exitoso", token});
  
  } catch (error) {
    console.error(error)
    return res.status(500).json({ msj: "error inesperado" })
  }
};

export default {login, register}
