import UserModel from '../models/users.mjs';

const profile = async (req, res) => {
  try {
    const user = await UserModel.findOne({_id: req.user.id})
    return res.status(200).json({ msj: "mi perfil", user});
  } catch (error) {
    console.log(error)
    return res.status(500).json({msj: "error inesperado"})
  }
};

export default {profile}
