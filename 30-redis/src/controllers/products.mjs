import ProductModel from '../models/products.mjs';

const my_products = async (req, res) => {
  try {
    const products = (req.params.id)? 
                        await ProductModel.findOne({_id: req.params.id, user: req.user.id}) 
                        : await ProductModel.find({user: req.user.id})

    return res.status(200).json({ products});
  } catch (error) {
    console.log(error)
    return res.status(500).json({msj: "error inesperado"})
  }
};

const add = async (req, res) => {
    try {
      let {title, description} = req.body
      const newProduct = new ProductModel({title, description, user: req.user.id})
      const product = await newProduct.save()
      return res.status(201).json({ product });
    } catch (error) {
      console.log(error)
      return res.status(500).json({msj: "error inesperado"})
    }
};

export default {my_products, add}
