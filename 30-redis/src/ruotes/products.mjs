import express from 'express';
const router = express.Router();
import productController from '../controllers/products.mjs'
import logged from '../middleware/logged.mjs'

router.use(logged)

router.get('/', productController.my_products)
router.get('/:id', productController.my_products)
router.post('/', productController.add)


export default router;