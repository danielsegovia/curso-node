import mongoose from "./cache.mjs";
import bcrypt from 'bcrypt'
import 'dotenv/config'

const productsSchema = new mongoose.Schema({
  title: String,
  description: String,
  user: mongoose.Schema.ObjectId
}, {timestamps: true});

const ProductModel = mongoose.model('products', productsSchema);

export default ProductModel
