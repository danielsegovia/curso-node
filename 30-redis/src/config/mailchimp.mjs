import 'dotenv/config'
import mailchimpFactory from "@mailchimp/mailchimp_transactional/src/index.js"
const mailchimp = mailchimpFactory(process.env.MAILCHIMP_TRANSACTIONAL_APIKEY);

export default mailchimp