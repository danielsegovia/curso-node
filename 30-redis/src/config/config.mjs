import 'dotenv/config'

const variables = {}
switch(process.env.NODE_ENV){
    case "test" : 
        variables.mongoURI = 'mongodb://localhost:27017/authTest02'
        break
    default: 
        variables.mongoURI = process.env.MONGO_URI
}

export default variables;