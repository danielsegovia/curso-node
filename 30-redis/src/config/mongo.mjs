import mongoose from 'mongoose'
import 'dotenv/config'
import config from './config.mjs'

// Establecemos la conexión con MongoDB
mongoose.connect(config.mongoURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Manejamos eventos de conexión y error

const db = mongoose.connection;

db.once('error', () => {
    console.error('Error de conexión')
    throw new Error("No se ha podido conectar a la DB: " + config.mongoURI)
    process.exit(1)
});

db.once('open', () => {
  console.log(`Mongo connected on ${config.mongoURI}`);
});

export default mongoose