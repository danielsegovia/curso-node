const fs = require('fs');
const data = fs.readFileSync('./users.txt', 'utf8');

let users = Array.isArray(JSON.parse(data))? JSON.parse(data) : []

const add = (firstname, lastname, email) => {

    try {

        const user = {
            firstname: firstname,
            lastname: lastname,
            email: email
        }

        users.push(user)
        save(JSON.stringify(users))
        return user

    } catch (error) {
        console.log(error)        
    }
}

const getAll = () => users


function save(content){
    try {
        fs.writeFileSync('./users.txt', content);
        return true
    } catch (err) {
        console.error(err);
        return false
    }
}




module.exports.add = add
module.exports.getAll = getAll