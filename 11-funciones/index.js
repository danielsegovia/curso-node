/* Definición */
function holaMundo(){
    console.log("Hola Mundo")
}

holaMundo(); //ejecución

const holaMundoFlecha = () => {
    console.log("Hola Mundo en función flecha")
}

holaMundoFlecha(); //ejecución

/* Fin definición */


/* Parametros */

function holaNombre(nombre){
    //recibo en nombre lo que envien como parámetro
    console.log("Hola " + nombre)
}


holaNombre("Dani"); //envió Dani a la función

const holaNombreFlecha = (nombre) => {
    console.log(`Hola ${nombre}`)
}

holaNombreFlecha("Mirta"); //ejecución

/* Fin parámetros */

/* return */

function suma(a, b) {
    return a + b
}

let resultado = suma(2,3)
console.log(`el resultado es: ${resultado}`)

const sumaFlecha = (a,b) => {
    return a + b
}
resultado = sumaFlecha(3,3)
console.log(`el resultado de sumaFlecha es: ${resultado}`)

//cuando tengo una sola operación no necesito abrir llaves ni tampoco utilizar return
const sumaReturnImplicito = (a,b) => a + b

resultado = sumaReturnImplicito(4,4)
console.log(`el resultado de sumaReturnImplicito es: ${resultado}`)

/* fin return */

//función flecha con 1 solo parámetro no es necesario abrir parentesis
const cuadrado = numero => numero*numero
resultado = cuadrado(4)
console.log(`el resultado de cuadrado es: ${resultado}`)

/* Scope */

scopeRegularFunction() // puede ejecutar antes de definirla

function scopeRegularFunction(){
    console.log("scopeRegularFunction global")
}

scopeRegularFunction() // puede ejecutar después de definirla

//Si la ejecuto antes de su definición no podrá acceder antes de su inicialización. Ver TDZ JavaScript
//scopeFlechaConst() // Error: Cannot access 'scopeFlechaConst' before initialization

const scopeFlechaConst = () => {
    console.log("scopeFlechaConst puedo ejecuarla después de su definición")
}

scopeFlechaConst()

//Mismo problema que con const, no puedo acceder antes de iniciarlizarla. Ver TDZ JavaScript
//scopeFlechaLet() // error: Cannot access 'scopeFlechaLet' before initialization

let scopeFlechaLet = () => {
    console.log("scopeFlechaLet  puedo ejecuarla después de su definición")
}

scopeFlechaLet() // puede ejecutar después de definirla

//scopeFlechaVar() // error: Cannot access 'scopeFlechaVar' before initialization

var scopeFlechaVar = () => {
    console.log("scopeFlechaVar  puedo ejecuarla después de su definición")
}

scopeFlechaVar() // puede ejecutar después de definirla

/* fin scope */

/*  Arguments  
function hola(a,b,c){
    console.log("============ HOLA ==============")
    console.log(arguments)
    console.log("============ ==== ==============")
}

const chau = (a,b,c) => {
    console.log("============ CHAU ==============")
    console.log(arguments)
    console.log("============ ==== ==============")
}

hola(1,3,5)
chau(2,4,6)


/*  Fin Arguments  */