if(true){
    console.log("lineas")
    console.log("de")
    console.log("codigo")
    console.log("para")
    console.log("expresión")
    console.log("verdadera")
}


if(false){
    console.log("esto")
    console.log("nunca")
    console.log("se")
    console.log("ejecutará")
}else{
    console.log("esto")
    console.log("SI")
    console.log("se")
    console.log("ejecutará")
}

const dayOfWeek = "martes";

switch (dayOfWeek) {
  case "lunes":
    console.log("Hoy es lunes. ¡Ánimo, que la semana recién comienza!");
    break;
  case "martes":
    console.log("Hoy es martes. ¡Ya superaste el primer día!");
    break;
  case "miércoles":
    console.log("Hoy es miércoles. ¡Estamos a mitad de semana!");
    break;
  case "jueves":
    console.log("Hoy es jueves. ¡Ya casi llegamos al viernes!");
    break;
  case "viernes":
    console.log("Hoy es viernes. ¡Al fin llegó el fin de semana!");
    break;
  default:
    //OJO, puede no ser fin de semana
    //si te envian dayOfWeek es "hola" ingresará por aquí de todas formas
    console.log("Estás de fin de semana, disfrútalo :-) ");
    
}