class Vehiculo {
    constructor(aceleracion, desaceleracion) {
        this.velocidad = 0
        this.aceleracion = aceleracion;
        this.desaceleracion = desaceleracion;
    }

    acelerar () {
        this.velocidad += this.aceleracion
    }

    frenar () {
        this.velocidad += this.desaceleracion
    }

    get velocidadActual() {
        return this.velocidad;
    }
}

const ferrari = new Vehiculo(5, 3);
const fiat = new Vehiculo(3, 2);

for (let i = 0; i < 10; i++) {
    ferrari.acelerar()
    fiat.acelerar()    
}

console.log(`La ferrari va a ${ferrari.velocidadActual}`)
console.log(`El fiat va a ${fiat.velocidadActual}`)

