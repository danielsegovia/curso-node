import 'dotenv/config'
import axios from 'axios'


export default {
    getWelcomeMessage : async (name) => {

        try {

            //return `${name} bienvenido a mi curso de Node.js`
        
            const apiKey = process.env.TEXTCORTEX_APIKEY; // Reemplaza con tu API key de OpenAI
            const endpoint = 'https://api.textcortex.com/v1/texts/paraphrases';
                            

            const payload = {
                "max_tokens": 512,
                "model": "chat-sophos-1",
                "n": 1,
                "source_lang": "es",
                "target_lang": "es",
                "temperature": 0.7,
                "text": `mensaje de bienvenida para ${name} al curso de Node.js` 
              }
            
            const response = await axios.post(endpoint, payload, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + apiKey
                },
            });

            if(response.status !== 200) return `${name} bienvenida/o al curso de Node.js`
        
            return response.data.data.outputs[0].text
        } catch (error) {
            console.error(error)
        }

    }
}