import KafkaConfig from "./config.js";
import mailing from './src/services/mailing.mjs'

// You can also use CommonJS `require('@sentry/node')` instead of `import`
import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";

Sentry.init({
  dsn: 'https://ad1ae49a6d0b5379f10e9b383dc5f61a@o4505897921085440.ingest.sentry.io/4505975483072512',
  integrations: [
    new ProfilingIntegration(),
  ],
  // Performance Monitoring
  tracesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
  // Set sampling rate for profiling - this is relative to tracesSampleRate
  profilesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
});

const kafkaConfig = new KafkaConfig();
kafkaConfig.consume("users", (value) => {

    const transaction = Sentry.startTransaction({
        op: "mailing",
        name: "Sending Welcome Email",
    });

    try {
        let user = JSON.parse(value)
        const response = mailing.sendWelcomeMessage(user.firstname)
        if(!response) throw new Error(`El email de bienvenida a ${user._id} no se ha podido enviar`)

    } catch (e) {
        console.error(e)
        Sentry.captureException(e);
      } finally {
        transaction.finish();
    }
    
    console.log("========= RECIBO MENSAJE EN OTRO APLICACION ============");
    console.log(value);
    console.log("========= FIN DE MENSAJE ============");
});
