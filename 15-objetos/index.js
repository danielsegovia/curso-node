const persona = {
    nombre: "Dani",
    apellido: "Sego"
}

console.log(persona)


// definir una funcion
const persona2 = {
    nombre: "Dani",
    apellido: "Sego",
    saludar: function(){
        console.log("hola mundo");
    }
}

console.log(persona2)
persona2.saludar()

// uso de this

const persona3 = {
    nombre: "Dani",
    apellido: "Sego",
    saludar: function(){
        console.log(`hola ${this.nombre} ${this.apellido}`);
    }
}

console.log(persona3)
persona3.saludar()

const persona3Arrow = {
    nombre: "Dani",
    apellido: "Sego",
    saludar: () =>{
        console.log(`hola ${this.nombre} ${this.apellido}`);
    }
}

console.log(persona3Arrow)
persona3Arrow.saludar() // hola undefined undefined

//objeto to string y string to objeto
const persona4 = {
    nombre: "Dani",
    apellido: "Sego"
}
const persona4Text = JSON.stringify(persona4)
console.log(typeof(persona4Text) + ": " + persona4Text)
const persona4Objeto = JSON.parse(persona4Text)
console.log(typeof(persona4Objeto) + ": " + persona4Objeto)
