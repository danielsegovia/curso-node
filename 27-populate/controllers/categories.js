const CategoryModel = require('../models/categories');

const get = async (req, res) => {
  try {
    const {id} = req.params
    const filter = (id)? {_id: id} : { }
    categories = await CategoryModel.find(filter)

    return res.status(200).json({ msj: "todos los cate23gorias", categories: categories });  
  } catch (error) {
    return res.status(500).json({msj: "error inesperado"})
  }
};

const search = async (req, res) => {
  try {
    const {text} = req.params
    console.log(text)

    const startDate = new Date();

    categories = await CategoryModel.find({description: "/" + text + "/"})

    const endDate = new Date();
    console.log(endDate - startDate)
  

    return res.status(200).json({ msj: "todos los categorias", categories: categories });  
  } catch (error) {
    console.log(error)
    return res.status(500).json({msj: "error inesperado"})
  }
};

const add = async (req, res) => {
  try {
    let category = new CategoryModel(req.body)
    category.save()
    return res.status(204).json({ category });  
  } catch (error) {
    return res.status(500).json({msj: "error inesperado"})
  }
};

module.exports = {add, get, search}
