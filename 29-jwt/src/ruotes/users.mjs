import express from 'express';
const router = express.Router();
import userController from '../controllers/users.mjs'
import logged from '../middleware/logged.mjs'

router.use(logged)

router.get('/profile', userController.profile)


export default router;