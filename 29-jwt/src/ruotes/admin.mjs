import express from 'express';
const router = express.Router();
import adminController from '../controllers/admin.mjs'
import logged from '../middleware/logged.mjs'
import admin from '../middleware/admin.mjs'

router.use(logged)
router.use(admin)

router.get('/', adminController.get)



export default router;