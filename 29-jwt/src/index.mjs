import 'dotenv/config'
import express from 'express';
import mongoose from './config/mongo.mjs';


const app = express();
app.use(express.json())
const port = process.env.PORT || 3000;

import usersRouter from './ruotes/users.mjs';
app.use('/users', usersRouter);
import authRouter from './ruotes/auth.mjs';
app.use('/auth', authRouter);
import adminRouter from './ruotes/admin.mjs';
app.use('/admin', adminRouter);

app.listen( port ,()=>{
    console.log(`server running on ${port}`)
});

