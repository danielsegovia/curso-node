# Suma sin centro

Dado un array de números sumar todos los elementos a excepción del valor del centro.

[1, 2, 3, 4, 5] = 12 (se elimina el 3)

[1, 2, 3, 4, 5, 6] = 14 (se eliminan 3 y 4)