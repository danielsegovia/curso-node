# Lenguaje Hacker

Enunciado del ejercicio: Implementación del algoritmo Bubble Sort

El objetivo de este ejercicio es implementar el algoritmo de ordenamiento Bubble Sort. El Bubble Sort es un algoritmo simple que ordena una lista de elementos comparando pares adyacentes y realizando intercambios si es necesario.

El objetivo es utilizar el mismo array por lo que está prohibido utilizar un array auxiliar

![Paso 1](https://media.geeksforgeeks.org/wp-content/uploads/20201222134207/bblsort1.png)
![Paso 2](https://media.geeksforgeeks.org/wp-content/uploads/20201222134231/bblsort2.png)
![Paso 3](https://media.geeksforgeeks.org/wp-content/uploads/20201222134233/bblsort3.png)
![Paso 4](https://media.geeksforgeeks.org/wp-content/uploads/20201222134257/bblsort4.png)
![Paso 5](https://media.geeksforgeeks.org/wp-content/uploads/20201222134259/bblsort5.png)
