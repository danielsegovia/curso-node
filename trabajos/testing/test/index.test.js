const calculadora = require('../calculadora')

describe("Calculadora", () =>{
    test("sumar", () => {
        const total = calculadora.suma(3,5)
        expect(total).toBe(8)
    })

    test("restar", () => {
        const total = calculadora.resta(7,5)
        expect(total).toBe(2)
    })

    test("dividir", () => {
        const total = calculadora.dividir(9,3)
        expect(total).toBe(3)
    })

    test("no division por cero", () => {
        const total = calculadora.dividir(9,0)
        //refactorizar el código para que no devuelva infinito
        expect(total).toBe(Infinity)
    })


})
