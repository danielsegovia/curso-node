if(!process.argv[2]){
    console.log("No enviaste ningún texto")
    process.exit(1)
}

const texto = process.argv[2]
const reSuma = /(suma)/i

if(!reSuma.test(texto)){
    console.log("El programa está diseñado para sumar 2 números, no pude detectar que quieras realizar está operación en el texto que me enviaste")
    process.exit(1)
}

const reNumeros = /-?\b\d+(?:\.\d+)?\b/g;
const matches = texto.match(reNumeros);

if (matches.length !== 2) {
    console.log("Solamente me diseñaron para sumar 2 números, si me envias menos de 2 o más de 2 no podré hacerlo")
    process.exit(1)
}

const numero1 = parseFloat(matches[0]);
const numero2 = parseFloat(matches[1]);

console.log(`El resultado de ${numero1}+${numero2} es ${numero1+numero2}`)
