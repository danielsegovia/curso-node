#partido de tenis

En el siguiente ejemplo se busca simular el tanteador de un partido de tenis

Para esto hay 2 jugadores P1 y P2

Genera un ciclo hasta que uno gane, por cada vuelta del ciclo cada jugador ganará un punto (genera el ganador de forma aleatoria)

El objetivo es mostrar los avances del tanteador.

Así es un juego de saque: 

P1- P2

15- 0

15- 15

30- 15

40- 15

40- 30

Iguales

Ventaja P1

Iguales

Ventaja P2

Ganador P2

En el game anterior dió como resultado que P2 ganó y en el tanteador es 1 a 0 en su favor

Completa el set teniendo con los diferentes games hasta llegar a 6

- Ten en cuenta que si el juego va 5 a 5 se juega se añade un game, por lo tanto el set será ganado por el jugador que llegue a 7
- Si el juego se empata en 6 cambia el sistema de puntación y será 1 punto por cada jugador que gane un punto hasta llegar a 7. Si el juego está empatado el ganador será quién saque una diferencia de 2

El ganador del partido será quién logre ganar 2 sets completos

Utiliza arrays y funciones para resolver el ejericicio