const to = (parseInt(process.argv[2]) > 0)? parseInt(process.argv[2]) : 100

for (let i = 1; i <= to; i++) {
    const isFizz = i % 3 === 0;
    const isBuzz = i % 5 === 0;
    console.log(isFizz || isBuzz ? (isFizz ? (isBuzz ? "FizzBuzz" : "Fizz") : "Buzz") : i);
}