if (!process.argv[2]) {
  console.log("« Ingresar número entero positivo como parámetro »");
  process.exit(1);
}

const numero = parseInt(process.argv[2]);

const regNumber = /\d/g;
if (!regNumber.test(numero) || numero <= 0) {
  console.log(`« Solo números ENTEROS positivos, no ingresar cero, negativos, tampoco letras, o signos »`);
  process.exit(1);
}

let intentos = 0;

if (numero === 1 || numero === 2) {
  intentos++;
  numero === 1
    ? console.log(`El número buscado es 1 y lo logré en ${intentos} intento`)
    : console.log(`El número buscado es 2 y lo logré en ${intentos} intento`);
  process.exit(1);
}

let maximo = 2;

do {
  maximo *= 2;
} while (numero > maximo);

let minimo = maximo / 2;

let adivino;
do {
  intentos++;
  let adivino = (minimo + maximo) / 2;
  adivino = Math.ceil((minimo + maximo) / 2);

  console.log(`minimo: ${minimo} - maximo: ${maximo} adivino: ${adivino}`);
  if (numero == adivino) {
    console.log(`El número buscado es ${adivino} y lo logré en ${intentos} intento`);
    break;
    process.exit(1);
  }

  if (numero > adivino) {
    minimo = adivino;
  } else {
    maximo = adivino;
  }
} while (numero !== adivino);
