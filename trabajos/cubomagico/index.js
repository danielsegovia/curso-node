//creo el cubo todo ordenado
const cubo = [
    Array(9).fill('naranja'), //cara frontal
    Array(9).fill('rojo'), //cara trasera
    Array(9).fill('verde'), //cara derecha
    Array(9).fill('blanco'), //cara izquierda
    Array(9).fill('amarillo'), //cara arriba
    Array(9).fill('azul') //cara abajo
]

//desordeno intercambiando 100 aleatoriamente cuadrados
//cubo[cara][cuadrado]
//cubo[random de 0 a 5][random de 0 a 9]
for (let i = 0; i < 100; i++) {

    [
        cubo[Math.floor(Math.random() * 6)][Math.floor(Math.random() * 9)], 
        cubo[Math.floor(Math.random() * 6)][Math.floor(Math.random() * 9)]
    ] 
    = 
    [
        cubo[Math.floor(Math.random() * 6)][Math.floor(Math.random() * 9)], 
        cubo[Math.floor(Math.random() * 6)][Math.floor(Math.random() * 9)]
    ];
}

console.log(cubo)
