const ProductModel = require('../models/products')

const my_products = async (req, res) => {
  try {
    const products = (req.params.id)? 
                        await ProductModel.findOne({_id: req.params.id}) 
                        : await ProductModel.find({})

    return res.status(200).json({ products});
  } catch (error) {
    next(error)
  }
};

const add = async (req, res, next) => {
    try {
      let {title, description} = req.body
      const newProduct = new ProductModel({title, description})
      const product = await newProduct.save()
      return res.status(201).json({ product });
    } catch (error) {
      next(error);
    }
};

module.exports = {my_products, add}
