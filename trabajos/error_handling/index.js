const express = require('express')
const app = express()
const mongoose = require('./config/mongo')

app.use(express.json())

const productRoutes = require('./ruotes/products')
app.use('/products', productRoutes);


app.use((err, req, res, next) =>{
  res.status(500).json({ error: err.message });
})


app.listen(7777)