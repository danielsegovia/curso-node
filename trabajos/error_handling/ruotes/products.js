const express = require('express')
const router = express.Router();
const productController = require('../controllers/products.js')

router.get('/', productController.my_products)
router.get('/:id', productController.my_products)
router.post('/', productController.add)


module.exports = router