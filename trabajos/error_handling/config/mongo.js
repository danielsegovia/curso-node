const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/error_handling', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.once('error', () => {
    console.error('Error de conexión:')
    process.exit(1)
});

db.once('open', () => {
  console.log('Conexión exitosa con la base de datos.');
});

module.exports = mongoose