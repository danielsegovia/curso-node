const mongoose = require('mongoose')

const productsSchema = new mongoose.Schema({
  title: { type: String, required: true, index: { unique: true } },
  description: String,
}, {timestamps: true});

const ProductModel = mongoose.model('products', productsSchema);

module.exports = ProductModel
