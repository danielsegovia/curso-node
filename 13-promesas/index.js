console.log("Arranco");
const randomNumber = (from=1, to=1000) => Math.floor(Math.random() * (to - from + 1)) + from;

const promesa1 = new Promise((resolve, reject) => {
    console.log("Inicio Promesa 1")
    const demora = randomNumber()
    setTimeout(() => {
        
        const number = randomNumber()
        if(number % 2 === 0){
            resolve(`Promesa1: El número ${number} es par y demoré ${demora}ms`)
        } else {
            reject(`Promesa1: BAD NEWS El número ${number} es impar y demoré ${demora}ms`)
        }
    }, demora);
})

promesa1
    .then(res => {
        console.log(res)
    })
    .catch(res => {
        console.log(res)
    })


const promesa2 = (demora, number) => new Promise((resolve, reject) => {
    console.log("Inicio Promesa 2")
    setTimeout(() => {
        
        if(number % 2 === 0){
            resolve(`Promesa2: El número ${number} es par y demoré ${demora}ms`)
        } else {
            reject(`Promesa2: BAD NEWS El número ${number} es impar y demoré ${demora}ms`)
        }
    }, demora);
    
})

promesa2(randomNumber(), randomNumber())
    .then(res => {
        console.log(res)
    })
    .catch(res => {
        console.log(res)
    })

