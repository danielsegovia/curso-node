import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";
import 'dotenv/config'
import express from 'express';
const app = express();
app.use(express.json())




const port = process.env.PORT || 3000;

Sentry.init({
    dsn: 'https://ac7f0db8943178bb3ff99d1569a81c14@o4505897921085440.ingest.sentry.io/4505897930391552'
  });


app.use(Sentry.Handlers.requestHandler());


app.use(Sentry.Handlers.tracingHandler());


app.get("/", function rootHandler(req, res) {
  res.end("Hello world!");
});

app.get("/debug-sentry", function mainHandler(req, res) {
    throw new Error("My first Sentry error!");
});

app.get('/ping', (req, res) => {
  res.status(200).json({msj: "app live"})
})


const users = []


app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
  res.status(500).json({msj: err.message});
});


app.listen( port ,()=>{
    console.log(`server running on ${port}`)
});

